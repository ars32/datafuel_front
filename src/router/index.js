import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/components/index'
import Profile from '@/components/profile'
import History from '@/components/history'
import Login from '@/components/login'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main
    },
    {
      path: '/profile/:id',
      name: 'Profile',
      component: Profile
    },
    {
      path: '/history',
      name: 'history',
      component: History
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    }
  ]
})
